package carlos.telles.processor;

import javax.xml.bind.ValidationException;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;

public class Application {

    public static void main(String[] args) {
        try {
            ConversorCoin conversorCoin = new ConversorCoin();
            BigDecimal conversor = conversorCoin.currencyQuotation("USD", "EUR", 100, "31/10/2017");
            System.out.println(conversor);
        } catch (IOException | ParseException | ValidationException e) {
            e.printStackTrace();
        }
    }


}
