package carlos.telles.processor;

import javax.xml.bind.ValidationException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Stream;

public class ConversorCoin {

    private static final String URL_CSV = "http://www4.bcb.gov.br/Download/fechamento/%s.csv";
    private DecimalFormat BR_FORMAT = setupBrazilFormat();

    private DecimalFormat setupBrazilFormat() {
        DecimalFormat format = new DecimalFormat();
        format.setMinimumFractionDigits(2);
        format.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(new Locale("pt", "BR")));
        return format;
    }

    public BigDecimal currencyQuotation(String from, String to, Number value, String quotation)
            throws IOException, ParseException, ValidationException {

        String stringUrl = URL_CSV;
        String[] dataSplit = quotation.split("/");
        String data = dataSplit[2] + dataSplit[1] + dataSplit[0];
        stringUrl = String.format(stringUrl, data);

        HashMap<String, HashMap> cotacoes = getQuotations(stringUrl);

        isValidCoin(cotacoes, from, to);
        isValidValue(value);

        return getCalcCurrency(from, to, value, cotacoes);
    }

    private void isValidValue(Number value) throws ValidationException {
        if (Objects.isNull(value) || value.doubleValue() < 0) {
            throw new ValidationException("The value is not valid");
        }
    }

    private void isValidCoin(HashMap<String, HashMap> cotacoes, String from, String to) throws ValidationException {
        if (Objects.isNull(cotacoes.get(from))) {
            throw new ValidationException("The currency is not valid");
        }

        if (Objects.isNull(cotacoes.get(to))) {
            throw new ValidationException("The currency is not valid");
        }
    }

    private BigDecimal getCalcCurrency(String from, String to, Number value, HashMap<String, HashMap> cotacoes)
            throws ParseException {

        Double rateFrom = BR_FORMAT.parse(cotacoes.get(from).get("rate").toString()).doubleValue();
        Double rateTo = BR_FORMAT.parse(cotacoes.get(to).get("rate").toString()).doubleValue();
        Double parity = BR_FORMAT.parse(cotacoes.get(to).get("parity").toString()).doubleValue();
        String type = cotacoes.get(to).get("type").toString();
        Double currency;

        if (isBRL(from)) {
            currency = calcDiv(value, rateTo);
        } else if (isBRL(to)) {
            currency = calc(value, rateFrom);
        } else if (isUSD(to)) {
            currency = calc(value, 1 / parity);
        } else {
            if (type.equals("A")) {
                currency = calc(value, parity);
            } else {
                currency = calcDiv(value, parity);
            }
        }

        return BigDecimal.valueOf(currency).setScale(2, RoundingMode.HALF_EVEN);
    }

    private Double calcDiv(Number value, Double parity) {
        return value.doubleValue() / parity;
    }

    private Double calc(Number value, Double rate) {
        return rate * value.doubleValue();
    }

    private boolean isBRL(String coin) {
        return coin.equals("BRL");
    }

    private boolean isUSD(String coin) {
        return coin.equals("USD");
    }

    private HashMap<String, HashMap> getQuotations(String stringUrl) throws IOException {
        try (InputStream arquivo = getArquivo(stringUrl);
             InputStreamReader reader = new InputStreamReader(arquivo);
             BufferedReader bufferedReader = new BufferedReader(reader);
             Stream<String> stream = bufferedReader.lines()) {
            HashMap<String, HashMap> cotacoes = new HashMap<>();

            stream.forEach((linha) -> {
                String[] cotacao = linha.split(";");

                HashMap<String, String> info = new HashMap<>();
                info.put("type", cotacao[2]);
                info.put("rate", cotacao[4]);
                info.put("parity", cotacao[6]);

                cotacoes.put(cotacao[3], info);
            });

            return cotacoes;
        }
    }

    private InputStream getArquivo(String stringUrl) throws IOException {
        URL url = new URL(stringUrl);

        return url.openStream();
    }
}
